public class App {
    public static void main(String[] args) throws Exception {
        AsymKrypt krypt = new AsymKrypt("keyName", true);

        // sign example
        String hash = "0xirgendwas";        
        byte[] signature = krypt.signData(hash);

        // verify it
        boolean isValid = AsymKrypt.verify(signature, hash.getBytes(), krypt.getPublicKey());
        System.out.printf("Daten passen zu Signatur: %s %n",isValid);

        // try with wrong data
        isValid = AsymKrypt.verify(signature, "falsche Daten".getBytes(), krypt.getPublicKey());
        System.out.printf("Daten passen zu Signatur: %s %n",isValid);
    }
}
